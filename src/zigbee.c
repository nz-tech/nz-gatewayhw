/***********************************************************************************
*
* MODULE:             XXX
* COMPONENT:          XXX
* REVISION:           $Revision: 0001
* DATED:              $Date: 2020-02-06 20:49
* AUTHOR:             Rax Jing
* EMAIL:              raxjing@163.com
*
************************************************************************************
*
*
* Copyright Smart Edge System 2019. All rights reserved.
*
***********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "zigbee.h"

static sensor_entry_t  SensorName[]={
    { E_SENSOR_NONE,            "Device Not Configured" },
    { E_SENSOR_TEMPERATURE,     "Temperature"           },
    { E_SENSOR_HUMIDITY,        "Humidity"              },
    { E_SENSOR_FIRE,            "Fire"                   },
    { E_SENSOR_GAS,             "Gas"                   },
    { E_SENSOR_MOTIONDET,       "Motiondet"             },
    { E_SENSOR_IR,              "IR Sensor"             },
    { E_SENSOR_SMOG,            "Smoke Sensor"          },
};

int get_sensor_name(unsigned char type,char *buf,int len)
{
    int i;
    for(i=0;i<sizeof(SensorName)/sizeof(sensor_entry_t);i++)
        if(type == SensorName[i].key){
            if(strlen(SensorName[i].value)<=len)
                memcpy(buf,SensorName[i].value,strlen(SensorName[i].value));
            else
                memcpy(buf,SensorName[i].value,len);
            return 0;
        }
    return -EINVAL;
}
