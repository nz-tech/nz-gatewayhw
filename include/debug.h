/***********************************************************************************
*
* MODULE:             Debug
* COMPONENT:          Debug
* REVISION:           $Revision: 0001
* DATED:              $Date: 2019-09-28 21:57
* AUTHOR:             Rax Jing
* EMAIL:              raxjing@163.com
*
************************************************************************************
*
* Copyright (c) 2019  Smart Edge System. All rights reserved
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* o Redistributions of source code must retain the above copyright notice, this list
*   of conditions and the following disclaimer.
*
* o Redistributions in binary form must reproduce the above copyright notice, this
*   list of conditions and the following disclaimer in the documentation and/or
*   other materials provided with the distribution.
*
* o Neither the name of the copyright holder nor the names of its
*   contributors may be used to endorse or promote products derived from this
*   software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
* ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
************************************************************************************/
#ifndef __SES_DEBUG_H__
#define __SES_DEBUG_H__


#include <syslog.h>

typedef enum Severity{
        EMERGENCY = LOG_EMERG,  ///< system is unusable
        ALERT = LOG_ALERT,      ///< action must be taken immediately
        CRITICAL = LOG_CRIT,    ///< critical conditions
        ERR = LOG_ERR,          ///< error conditions
        WARNING = LOG_WARNING,  ///< warning conditions
        NOTICE = LOG_NOTICE,    ///< normal but significant condition
        INFO = LOG_INFO,        ///< informational
        DEBUG = LOG_DEBUG,      ///< debug-level messages
} log_level_e;


#define CONSOLE ///< show information on console

/// This determines the importance and priority of the message.
/// Whatever will be the priority defined, It will enable
/// all higher  priority messages above the defined one.
#define ENABLE_DBG_LVL INFO

#if defined( CONSOLE ) && defined( ENABLE_DBG_LVL )
#define LOG( log_level, msg, ... ) \
{\
        if ( log_level <= ENABLE_DBG_LVL ) \
        {\
                syslog( log_level, "%s:%d: "msg, __func__, __LINE__, ##__VA_ARGS__ );\
                printf( "[%s]:[%d]:"msg" ", __func__, __LINE__, ##__VA_ARGS__ );\
                fflush( stdout ); \
        }\
}
#elif defined( ENABLE_DBG_LVL )
#define LOG( log_level, msg, ... ) \
{\
        if ( log_level <= ENABLE_DBG_LVL ) \
        {\
                syslog( log_level, "%s:%d: "msg, __func__, __LINE__, ##__VA_ARGS__ );\
        }\
}
#endif // ENABLE_DBG_LVL/CONSOLE

/// Function entry-exit prints
#define FUNC_ENTRY() LOG( INFO, "Entered : %s\n", __FUNCTION__ )
#define FUNC_EXIT()  LOG( INFO, "Exited : %s\n", __FUNCTION__ )


#endif //__SES_DEBUG_H__
