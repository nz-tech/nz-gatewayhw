/***********************************************************************************
*
* MODULE:             Zigbee head file
* COMPONENT:          Zigbee
* REVISION:           $Revision: 1.0
* DATED:              $Date: 2019-10-07 23:28
* AUTHOR:             Rax Jing
* EMAIL:              raxjing@163.com
*
************************************************************************************
*
* Zigbee common include file
*
* Copyright Smart Edge System 2019. All rights reserved.
*
***********************************************************************************/
#ifndef __SES_ZIGBEE_H__
#define __SES_ZIGBEE_H__
#if defined __cplusplus
extern "C" {
#endif

#include <pthread.h>

#define ZIGBEE_MAX_CONNECTED_DEVICE         (4)
#define ZIGBEE_CMD_HEAD_USER_DEFINED        (0x5A)
#define ZIGBEE_MAX_MESSAGE_LENGTH           (64)
#define INVALID_CONNECTED_DEVICE_ENTRY      (0U)
#define VALID_CONNECTED_DEVICE_ENTRY        (1U)
#define INVALID_SHORT_ADDRESS               (0xFFFFU)

#define DEV_MGMT_TICK_INTERVAL_MS           ( 100U ) ///< Tick interval in milli seconds
#define DEV_MGMT_ONE_SEC_TICK_CNT           ( 1000U / DEV_MGMT_TICK_INTERVAL_MS )

#define DEV_DISC_STATE_TICK_INTERVAL_MS     ( 2U * DEV_MGMT_TICK_INTERVAL_MS )
#define DEV_DISC_STATE_ONE_SEC_TICK_CNT     ( 1000 / DEV_DISC_STATE_TICK_INTERVAL_MS )


#define WAIT_DELAY_STATUS_RESPONSE_MS       ( 500 )
#define ZGB_WAIT_DELAY_STATUS_RESPONSE_MS   (5000)
#define ZIGBEE_WAIT_TIMEOUT_DEFAULT         ( 30 * DEV_DISC_STATE_ONE_SEC_TICK_CNT)
#define ZIGBEE_TIMEOUT_RETRY_DEFAULT        (20)

#define     ZIGBEE_FIFO     "/tmp/fifoZigbee"
#define     ZIGBEE_SOCKET   "/tmp/coordinator-socket"


/* Structure used to contain a Serial Link message */
typedef struct InterfaceMessage
{
    unsigned char u8CmdId;
    unsigned char u8Head;     /* response Head */
    unsigned char u8Length;   /* response data length */
    unsigned char u8Message[ZIGBEE_MAX_MESSAGE_LENGTH];
    unsigned char u8Status;
} if_message_t;

/* Structure to store SL message which comprises of ID and SL message text. */
typedef struct InterfaceMessageBuff
{
    long            mtype;
    if_message_t    if_message;
} if_message_buf_t;


typedef struct InterfaceControlBlock
{
    char    dev[32];
    int     fd;
    unsigned char status;
    unsigned char u8SeqNo;
    unsigned char u8CmdId;
    unsigned char u8RespHead;
    unsigned char u8RespDataLen;
    pthread_mutex_t write_lock;
    pthread_mutex_t read_lock;           /* lock sl read message */
    pthread_cond_t  read_available;
    unsigned int response;                          /* pure response if not data */
    void    (*lock_write)(void);
    int     (*write)(unsigned char *buf,int len);
    void    (*unlock_write)(void);
    int     (*wait_for_reception)(unsigned char cmd);
    int     (*read)(if_message_t *pslMsg,int MaxLength);
    void    (*lock_read)(void);         /*  */
    void    (*unlock_read)(void);
} zigbee_if_cb_t;


typedef enum teSL_RxState
{
    E_STATE_RX_WAIT_START,
    E_STATE_RX_WAIT_LEN,
    E_STATE_RX_WAIT_DATA,
    E_STATE_RX_WAIT_CRC,
} sl_rx_state_e;


#define E_ZGB_MSG_TYPE_ANY   (0xFF)

/* Enumerated type of Zigbee broadcast addresses */
typedef enum ZigbeeBroadcastAddress
{
    E_ZGB_BROADCAST_ADDRESS_ALL              = 0xFFFF,
    E_ZGB_BROADCAST_ADDRESS_RXONWHENIDLE     = 0xFFFD,
    E_ZGB_BROADCAST_ADDRESS_ROUTERS          = 0xFFFC,
    E_ZGB_BROADCAST_ADDRESS_LOWPOWERROUTERS  = 0xFFFB,
} zigbee_broadcast_address_e;

typedef enum EndpointWorkMode
{
  E_DEVICE_MODE_TIMER   = 0x01,
  E_DEVICE_MODE_POLLING = 0x02,
}endpoint_workmode_e;

typedef struct SensorBoardZigbeeInfo{
    unsigned char   SensorType;
    unsigned char   WorkMode;
    unsigned char   HwType;
    short           BuildNum;
    unsigned char   Channel;
    char            rssi;
    char            ChipTemp;       /* temperture */
    unsigned char   ChipVolt;       /* voltage */
    unsigned short  ShortAddr;
    long long       MacAddr;
    unsigned int    PeriodTime;
    char            SensorData;
}zigbee_board_info_t;


typedef struct ZigbeeEndpointInformation{
    unsigned char   name[32];
    /* data stream from zigbee endpoint */
    unsigned char   SensorType;
    unsigned char   WorkMode;
    unsigned char   HwType;
    unsigned short  BuildNum;
    unsigned char   Channel;
    char            rssi;
    char            ChipTemp;       /* temperture */
    unsigned char   ChipVolt;       /* voltage */
    unsigned short  ShortAddr;
    long long       MacAddr;
    unsigned int    PeriodTime;
    char            SensorData;
}zigbee_endpoint_info_t;

/* Structure sto store the information of list of connected device */
typedef struct ZigbeeEndpoint
{
    unsigned char           Valid;
    unsigned char           is_announced;
    unsigned char           DeviceState;
    unsigned char           CurStateWaitTicks;
    unsigned char           CurStateRetryCnt;
    zigbee_endpoint_info_t  ep;
} zigbee_endpoint_t;

typedef struct ZigbeeConnectedDeviceList{
    unsigned char           OnlineEndpointNum;
    zigbee_endpoint_info_t  ep[ZIGBEE_MAX_CONNECTED_DEVICE];
}zigbee_connected_device_list_t;

typedef struct ZigbeeModuleInformation{
    char            name[16];
    char            DeviceType[32];
    unsigned char   OnlineEndpointNum;
    unsigned short  DestShortAddr;
    unsigned short  PanId;
    unsigned short  DefaultPanId;
    unsigned short  ShortAddr;
    unsigned char   Channel;
    long long       MacAddr;
    char            ChipTemp;       /* temperture */
    unsigned char   ChipVolt;       /* voltage */
    unsigned char   RadioPower;
    char            ProductTypeVersion[16];
    unsigned int    PeriodTime;
    char            SoftwareVersion[16];
    unsigned short  BuildNum;
    unsigned char   UserData[4];
}zigbee_module_info_t;

typedef struct ZigbeeModule{

    pthread_mutex_t info_lock;   /* lock protection for module info */
    zigbee_module_info_t info;

    zigbee_if_cb_t  if_cb;
    void    (*lock)(void);
    void    (*unlock)(void);
    int     (*init)(char *path,int baud);
    int     (*self_checking)(void);
    int     (*disconnect)(void);
    int     (*is_vendor_std_head)(unsigned char head);

    int     (*get_DestShortAddr)(unsigned short *addr);
    int     (*set_DestShortAddr)(unsigned short addr);

    int     (*get_PanId)(unsigned short *id);
    int     (*set_PanId)(unsigned short id);

    int     (*get_ShortAddr)(unsigned short *addr);
    int     (*set_ShortAddr)(unsigned short addr);

    int     (*get_Channel)(unsigned char *ch);
    int     (*set_Channel)(unsigned char addr);

    int     (*get_DeviceType)(char *dt,int len);
    int     (*set_DeviceType)(char *dt);

    int     (*get_MacAddr)(long long *mac);
//    int     (*set_MacAddr)(long long mac);

    int     (*get_RSSI)(unsigned int *rssi); /* address + rssi in *rssi */
    int     (*get_ChipTemp)(char *temp);
    int     (*get_ChipVolt)(unsigned char *volt);

    /* extended commands */
    int     (*get_ProductTypeVersion)(char *ptv,int len);
    int     (*set_BaudRate)(unsigned int baud);
    int     (*set_RadioPower)(unsigned char pwr);
    int     (*set_Reboot)(void);
    int     (*set_FactoryReset)(void);

    int     (*get_PeriodTime)(unsigned int *pdat);
    int     (*set_PeriodTime)(unsigned int pdat);

    int     (*get_UserData)(unsigned char idx,unsigned char *pdat);
    int     (*set_UserData)(unsigned char idx,unsigned char pdat);

    /* special commands */
    int     (*get_SoftwareVersion)(char *sw,int len);
    int     (*get_BuildNum)(unsigned short *nb);
    int     (*get_UserInfo)(char *inf,int len);
    int     (*set_UserInfo)(unsigned char *inf,int len);
}zigbee_module_t;

typedef struct ZigbeeCtlBlk{
    pthread_mutex_t         lock_manager;
    pthread_mutex_t         lock_deviceList;
    zigbee_module_t         *module;
    int                     wait_timeout;   /* second */
    int                     timeout_retry;  
    zigbee_endpoint_t       device[ZIGBEE_MAX_CONNECTED_DEVICE];
} zigbee_manager_t;


typedef struct ZigbeeUserData{
    unsigned char   index;
    unsigned char   data;
}zigbee_userdata_t;

typedef struct ZigbeeSensorData{
    unsigned short  addr;
    char            data;
}zigbee_sensor_data_t;


typedef union ZigbeeCmdData{
    unsigned char                   buf[ZIGBEE_MAX_MESSAGE_LENGTH];
    zigbee_module_info_t            module;
    zigbee_endpoint_info_t          endpoint;
    zigbee_connected_device_list_t  ConnectedDevice;
    zigbee_sensor_data_t            SensorData;
    char                            DeviceType[32];
    unsigned short                  DestShortAddr;  /* Module */
    unsigned short                  PanId;
    unsigned short                  ShortAddr;
    unsigned char                   Channel;
    long long                       MacAddr;
    char                            ChipTemp;       /* temperture */
    unsigned char                   ChipVolt;       /* voltage */
    unsigned int                    BaudRate;
    unsigned char                   RadioPower;
    char                            ProductTypeVersion[16];
    unsigned int                    PeriodTime;
    char                            SoftwareVersion[16];
    unsigned short                  BuildNum;
    zigbee_userdata_t               UserData;
    unsigned char                   UserInfo[64];
}zigbee_cmd_data_t;

/* Structure used to contain a  request */
typedef struct ZigbeeMessage {
    unsigned char       id;         /* sequence number */
    unsigned char       cmd;        /* NZTech defined zigbee command */
    unsigned short      addr;       /* target endpoint address */
    zigbee_cmd_data_t   data;       /* command data */
    unsigned char       datalen;    /* data length */
    char                status;
} zigbee_message_t;

typedef struct ZigbeeMessageBuf
{
    long                mtype;
    zigbee_message_t    msg;
} zigbee_message_buf_t;


#define ZIGBEE_CMD(is_ep,cmd)     (((is_ep)?0x80:0)|(cmd))

typedef enum ZigbeeCmd {
    /* the on board module commands */
    ZGB_MODULE_GET_INFO             = ZIGBEE_CMD(0,0x00),

    ZGB_MODULE_GET_DEVICETYPE       = ZIGBEE_CMD(0,0x10),
    ZGB_MODULE_SET_DEVICETYPE       = ZIGBEE_CMD(0,0x11),

    ZGB_MODULE_GET_PANID            = ZIGBEE_CMD(0,0x12),
    ZGB_MODULE_SET_PANID            = ZIGBEE_CMD(0,0x13),

    ZGB_MODULE_GET_SHORTADDR        = ZIGBEE_CMD(0,0x14),
    ZGB_MODULE_SET_SHORTADDR        = ZIGBEE_CMD(0,0x15),

    ZGB_MODULE_GET_MACADDR          = ZIGBEE_CMD(0,0x16),
    ZGB_MODULE_SET_MACADDR          = ZIGBEE_CMD(0,0x17),

    ZGB_MODULE_GET_DESTADDR         = ZIGBEE_CMD(0,0x18),
    ZGB_MODULE_SET_DESTADDR         = ZIGBEE_CMD(0,0x19),

    ZGB_MODULE_SET_REBOOT           = ZIGBEE_CMD(0,0x1A),
    ZGB_MODULE_SET_FACTORY_RESET    = ZIGBEE_CMD(0,0x1B),

    ZGB_MODULE_GET_PERIODTIME       = ZIGBEE_CMD(0,0x1C),
    ZGB_MODULE_SET_PERIODTIME       = ZIGBEE_CMD(0,0x1D),

    ZGB_MODULE_GET_USERDATA         = ZIGBEE_CMD(0,0x1E),
    ZGB_MODULE_SET_USERDATA         = ZIGBEE_CMD(0,0x1F),

    ZGB_MODULE_GET_CHIPTEMP         = ZIGBEE_CMD(0,0x20),
    ZGB_MODULE_GET_CHIPVOLT         = ZIGBEE_CMD(0,0x21),

    ZGB_MODULE_SET_BAUDRATE         = ZIGBEE_CMD(0,0x22),
    ZGB_MODULE_SET_RADIOPWR         = ZIGBEE_CMD(0,0x23),
    ZGB_MODULE_GET_PRODUCTVERSION   = ZIGBEE_CMD(0,0x24),
    ZGB_MODULE_GET_SWVERSION        = ZIGBEE_CMD(0,0x25),
    ZGB_MODULE_GET_BUILDNUM         = ZIGBEE_CMD(0,0x26),

    ZGB_MODULE_GET_USERINFO         = ZIGBEE_CMD(0,0x28),
    ZGB_MODULE_SET_USERINFO         = ZIGBEE_CMD(0,0x29),

    ZGB_MODULE_SET_CHANNEL          = ZIGBEE_CMD(0,0x30),

    /* endpoint commands */
    ZGB_ENDPOINT_GET_LIST           = ZIGBEE_CMD(1,0x00),
    ZGB_ENDPOINT_GET_INFO           = ZIGBEE_CMD(1,0x01),

    ZGB_ENDPOINT_GET_DEVICETYPE     = ZIGBEE_CMD(1,0x10), /* endpint is endpoint */
    ZGB_ENDPOINT_SET_DEVICETYPE     = ZIGBEE_CMD(1,0x11), /* not supported */

    ZGB_ENDPOINT_GET_PANID          = ZIGBEE_CMD(1,0x12), /* not necessary */
    ZGB_ENDPOINT_SET_PANID          = ZIGBEE_CMD(1,0x13), /* no response */

    ZGB_ENDPOINT_GET_SHORTADDR      = ZIGBEE_CMD(1,0x14), /* not necessary */
    ZGB_ENDPOINT_SET_SHORTADDR      = ZIGBEE_CMD(1,0x15),

    ZGB_ENDPOINT_GET_MACADDR        = ZIGBEE_CMD(1,0x16), /* not necessary */
    ZGB_ENDPOINT_SET_MACADDR        = ZIGBEE_CMD(1,0x17), /* not supported */

    ZGB_ENDPOINT_GET_DESTADDR       = ZIGBEE_CMD(1,0x18), /* not necessary */
    ZGB_ENDPOINT_SET_DESTADDR       = ZIGBEE_CMD(1,0x19), /* not necessary */

    ZGB_ENDPOINT_SET_REBOOT         = ZIGBEE_CMD(1,0x1A),
    ZGB_ENDPOINT_SET_FACTORY_RESET  = ZIGBEE_CMD(1,0x1B),

    ZGB_ENDPOINT_GET_PERIODTIME     = ZIGBEE_CMD(1,0x1C), /* not necessary */
    ZGB_ENDPOINT_SET_PERIODTIME     = ZIGBEE_CMD(1,0x1D),

    ZGB_ENDPOINT_GET_USERDATA       = ZIGBEE_CMD(1,0x1E),
    ZGB_ENDPOINT_SET_USERDATA       = ZIGBEE_CMD(1,0x1F),

    ZGB_ENDPOINT_GET_CHIPTEMP       = ZIGBEE_CMD(1,0x20),
    ZGB_ENDPOINT_GET_CHIPVOLT       = ZIGBEE_CMD(1,0x21),

    ZGB_ENDPOINT_SET_BOUDRATE       = ZIGBEE_CMD(1,0x22), /* not supported */
    ZGB_ENDPOINT_SET_RADIOPWR       = ZIGBEE_CMD(1,0x23), /* not supported */
    ZGB_ENDPOINT_GET_PRODUCTVERSION = ZIGBEE_CMD(1,0x24),
    ZGB_ENDPOINT_GET_SWVERSION      = ZIGBEE_CMD(1,0x25),
    ZGB_ENDPOINT_GET_BUILDNUM       = ZIGBEE_CMD(1,0x26),

    ZGB_ENDPOINT_GET_USERINFO       = ZIGBEE_CMD(1,0x28),
    ZGB_ENDPOINT_SET_USERINFO       = ZIGBEE_CMD(1,0x29), /* not supported */

    ZGB_ENDPOINT_SET_CHANNEL        = ZIGBEE_CMD(1,0x30),
    ZGB_ENDPOINT_CFG_LOWPWR         = ZIGBEE_CMD(1,0x31),/* 0:disable, 1 enable */
    ZGB_ENDPOINT_LOWPWR_TIMEOUT     = ZIGBEE_CMD(1,0x32),/* once timeout, low power enabled */

    ZGB_ENDPOINT_OFFLINE            = ZIGBEE_CMD(1,0x40),
    ZGB_ENDPOINT_CONNECT            = ZIGBEE_CMD(1,0x41),
    ZGB_ENDPOINT_ANNOUNCE           = ZIGBEE_CMD(1,0x42),
    ZGB_ENDPOINT_ONLINE             = ZIGBEE_CMD(1,0x43),
    ZGB_ENDPOINT_DATA               = ZIGBEE_CMD(1,0x44),
    ZGB_ENDPOINT_GPIO_READ          = ZIGBEE_CMD(1,0x45),
    ZGB_ENDPOINT_GPIO_WRITE         = ZIGBEE_CMD(1,0x46),
    ZGB_ENDPOINT_GPIO_CONFIG        = ZIGBEE_CMD(1,0x47),

    ZGB_ENDPOINT_TEST               = ZIGBEE_CMD(1,0x51),

}zigbee_cmd_e;


typedef enum EndpointCommandResponseCode{
    ZGB_ENDPOINT_CMD_SUCCESS        = 0x00,
    ZGB_ENDPOINT_CMD_FAIL           = 0xE1,
    ZGB_ENDPOINT_CRC_FAIL           = 0xE2,
    ZGB_ENDPOINT_INVALID_PAR        = 0xE3,
    ZGB_ENDPOINT_NOT_SUPPORTED      = 0xE4,
}endpoint_err_e;

typedef struct SensorEntry{
    int     key;
    char    *value;
}sensor_entry_t;


#define SENSOR_TEMPERATURE      0
#define SENSOR_HUMIDITY         1
#define SENSOR_FIRE             2
#define SENSOR_GAS              3
#define SENSOR_MOTIONDET        4
#define SENSOR_IR               5
#define SENSOR_SMOG             6
#define SENSOR_RESERVE          7

#define SENSOR_TEMPERATURE_MASK (1<<SENSOR_TEMPERATURE)
#define SENSOR_HUMIDITY_MASK    (1<<SENSOR_HUMIDITY)
#define SENSOR_FIRE_MASK        (1<<SENSOR_FIRE)
#define SENSOR_GAS_MASK         (1<<SENSOR_GAS)
#define SENSOR_MOTIONDET_MASK   (1<<SENSOR_MOTIONDET)
#define SENSOR_IR_MASK          (1<<SENSOR_IR)
#define SENSOR_SMOG_MASK        (1<<SENSOR_SMOG)
#define SENSOR_RESERVE_MASK     (1<<SENSOR_RESERVE)


typedef enum SensorType
{
    E_SENSOR_NONE           = 0,
    E_SENSOR_TEMPERATURE    = SENSOR_TEMPERATURE,
    E_SENSOR_HUMIDITY       = SENSOR_HUMIDITY,
    E_SENSOR_FIRE           = SENSOR_FIRE,
    E_SENSOR_GAS            = SENSOR_GAS,
    E_SENSOR_MOTIONDET      = SENSOR_MOTIONDET,
    E_SENSOR_IR             = SENSOR_IR,
    E_SENSOR_SMOG           = SENSOR_SMOG,
    E_SENSOR_RESERVE        = SENSOR_RESERVE,
} sensor_type_e;

typedef enum
{
    LRF215A     =   0x01,
    LRF215A_PA  =   0x02,
    LRF215A1    =   0x03,
    LRF215C     =   0x04,
    LRF215C_PA  =   0x05,
    LRF215U     =   0x06,
    LRF215U_PA  =   0x07,
    LRF215AB    =   0x31
}KELAN_HW_TYPE;

zigbee_module_t *get_zigbee_module(void);
int get_sensor_name(unsigned char type,char *buf,int len);

//from zigbee control.h
#if defined __cplusplus
}
#endif

#endif /*__SES_ZIGBEE_H__*/
